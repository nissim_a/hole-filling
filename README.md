# Hole filling

## Usage
Just run the jar 'hole-filling' with your command line of your choice.

The available commands:

### Command

**fill-hole**


    Arguments:
        -f source file. Required
        -d destination file. Required
        -m mask file. Required
        -e epsilon. Optionaly default value: 0.000001
        -z Optionaly default value: 5
        -c pixel connectivity.  Optionaly default value: 8
        -b show boundary pixels. Optionaly default false
        -r find first hole pixel randomly. Optionaly default false

**Example**
```
$ java -jar hole-filling.jar fill-hole -f Dog-1024x576.jpg -d Dog-1024x576-gray.jpg -m mask.jpg -c 8 -e 0.0000001 -z 4
```

## Algorithm complexity

### Default algorithm complexity.
We assume that the hole and boundary were already found. 

Let m be the number of pixels in boundary pixels, and n pixels inside the hole.

The Weight function (w(u, v)) computation’s time is O(1) because it's just a few computations operations.

I(u) computation’s time is also O(1) as we just need to get the value on the matrix in the coordinate u = (ux, uy) that is done in O(1).

For each pixel in the hole we need to sum w(u, v) * I(v) that is done in O(m) as we have m boundary pixels. 

And sum w(u, v) which is also done in O(m).

And then divide the two. So we get a complexity of O(m).

As we need to calculate it for n pixels, the total complexity is O(n * m).

#### Expressing the complexity only in terms of n.

Looking at the perimeter of the hole, a 'line' will have the biggest boundary, therefore m is bounded by 2n+6 lets round it to 3n. 
Now if we want to express the complexity in terms of only n, we will get  O(n * m) < O(n * 3n) = O(n^2).

### An algorithm that approximates the result in O(n).
About the weight function, as much as the distance of two pixels is big, the distant pixel color contribution is close to zero. 
So, instead of running over all the boundary pixels, we can just see the color of the 8 connected pixels to the hole.

This way instead of iterating over m pixels of the boundary for each pixel in the hole we only iterate over 8 so we get O(8 * n) = O(n).