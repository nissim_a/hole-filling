package com.nissimarfi.holefilling.lib;

/**
 * An object which represents a hole in an image.
 */
public class Hole {

    private Iterable<Point> mBoundaryPoints;
    private Iterable<Point> mHolePoints;

    public Hole(Iterable<Point> boundaryPoints, Iterable<Point> holePoints) {
        mBoundaryPoints = boundaryPoints;
        mHolePoints = holePoints;
    }

    public Iterable<Point> getBoundaryPoints() {
        return mBoundaryPoints;
    }

    public Iterable<Point> getHolePoints() {
        return mHolePoints;
    }
}
