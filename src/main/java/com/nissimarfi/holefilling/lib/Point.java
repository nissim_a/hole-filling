package com.nissimarfi.holefilling.lib;

/**
 * Represent a coordinate
 */
public class Point {
    private int mX;
    private int mY;

    public Point(int x, int y) {
        mX = x;
        mY = y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Point)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        Point p = (Point) obj;
        return mX == p.getX() && mY == p.getY();
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + mX;
        result = 31 * result + mY;
        return result;
    }

    public int getX() {
        return mX;
    }

    public int getY() {
        return mY;
    }
}
