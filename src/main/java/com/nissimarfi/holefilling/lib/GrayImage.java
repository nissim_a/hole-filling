package com.nissimarfi.holefilling.lib;

public class GrayImage {

    /**
     * Represents a missing color of a pixel
     */
    public static int MISSING_COLOR_VALUE = -1;

    /**
     * The width of the image
     */
    private int mWidth;

    /**
     * The height of the image
     */
    private int mHeight;

    /**
     * The pixels of the image
     */
    private float[][] mPixels;


    /**
     * Creates a {@code width}-by-{@code height} image, with {@code width} columns
     * and {@code height} rows, where each pixel is black.
     *
     * @param width  the width of the image
     * @param height the height of the image
     * @throws IllegalArgumentException if {@code width} is negative
     * @throws IllegalArgumentException if {@code height} is negative
     */
    public GrayImage(int width, int height) {
        if (width < 0) throw new IllegalArgumentException("width must be non-negative");
        if (height < 0) throw new IllegalArgumentException("height must be non-negative");

        mWidth = width;
        mHeight = height;
        mPixels = new float[height][width];
    }

    public GrayImage(float[][] pixels) throws InvalidImageException {
        if (pixels == null || pixels[0] == null) {
            throw new InvalidImageException();
        }

        validatePixels(pixels);

        mWidth = pixels.length;
        mHeight = pixels[0].length;
        mPixels = pixels;
    }

    /**
     * Creates a new grayscale image that is a deep copy of the argument image.
     *
     * @param toCopy the image to copy
     * @throws IllegalArgumentException if {@code image} is {@code null}
     */
    public GrayImage(GrayImage toCopy) {
        if (toCopy == null) throw new IllegalArgumentException("constructor argument is null");

        mWidth = toCopy.getWidth();
        mHeight = toCopy.getHeight();
        mPixels = new float[mHeight][mWidth];

        for (int row = 0; row < mHeight; row++) {
            for (int col = 0; col < mWidth; col++) {
                mPixels[row][col] = toCopy.getPixel(col, row);
            }
        }
    }

    /**
     * Check if the pixel is a hole
     *
     * @param col the column index
     * @param row the row index
     * @return true if it is a hole false otherwise
     * @throws IllegalArgumentException unless both {@code 0 <= col < width} and {@code 0 <= row < height}
     */
    public boolean isHole(int col, int row) {
        return getPixel(col, row) == MISSING_COLOR_VALUE;
    }

    /**
     * Returns the height of the image.
     *
     * @return the height of the image (in pixels)
     */
    public int getHeight() {
        return mHeight;
    }

    /**
     * Returns the width of the image.
     *
     * @return the width of the image (in pixels)
     */
    public int getWidth() {
        return mWidth;
    }

    /**
     * Sets the color of pixel ({@code col}, {@code row}) to the given grayscale value.
     *
     * @param col the column index
     * @param row the row index
     * @param color the color (converts to grayscale if color is not a shade of gray)
     * @throws IllegalArgumentException unless both {@code 0 <= col < width} and {@code 0 <= row < height}
     * @throws IllegalArgumentException unless {@code color == -1} or {@code 0 <= color <= 1}
     */
    public void setPixel(int col, int row, float color) {
        validateColumnIndex(col);
        validateRowIndex(row);
        validateGrayscaleValue(color);
        mPixels[row][col] = color;
    }

    /**
     * Returns the grayscale value of pixel ({@code col}, {@code row}).
     *
     * @param col the column index
     * @param row the row index
     * @return the grayscale value of pixel ({@code col}, {@code row})
     * @throws IllegalArgumentException unless both {@code 0 <= col < width} and {@code 0 <= row < height}
     */
    public float getPixel(int col, int row) {
        validateColumnIndex(col);
        validateRowIndex(row);
        return mPixels[row][col];
    }

    /**
     * Sets the color of pixel ({@code col}, {@code row}) to the given grayscale value
     * between 0 and 255 or -1 represents a missing color of a pixel.
     *
     * @param col the column index
     * @param row the row index
     * @param gray the 8-bit integer representation of the grayscale value
     * @throws IllegalArgumentException unless both {@code 0 <= col < width} and {@code 0 <= row < height}
     */
    public void setGrayscale(int col, int row, int gray) {
        gray = (gray == -1) ? -255 : gray & 0xff;
        setPixel(col, row, gray / 255f);
    }

    /**
     * Returns the grayscale value of pixel ({@code col}, {@code row}) as an {@code int}
     * between 0 and 255 or -1 for a pixel that's color is missing.
     *
     * @param col the column index
     * @param row the row index
     * @return the 8-bit integer representation of the grayscale value of pixel ({@code col}, {@code row})
     * @throws IllegalArgumentException unless both {@code 0 <= col < width} and {@code 0 <= row < height}
     */
    public int getGrayscale(int col, int row) {
        float pixel = getPixel(col, row);
        return (pixel == -1) ? -1 : (int) (pixel * 255);
    }

    private void validateRowIndex(int row) {
        if (row < 0 || row >= getHeight()) {
            throw new IllegalArgumentException("row index must be between 0 and " + (getHeight() - 1) + ": " + row);
        }
    }

    private void validateColumnIndex(int col) {
        if (col < 0 || col >= getWidth()) {
            throw new IllegalArgumentException("column index must be between 0 and " + (getWidth() - 1) + ": " + col);
        }
    }

    private void validateGrayscaleValue(float gray) {
        if (gray != -1 && (gray < 0 || gray > 1)) {
            throw new IllegalArgumentException("grayscale value must be between 0 and 1 or -1");
        }
    }

    private void validatePixels(float[][] pixels) throws InvalidImageException {
        for (int row = 0; row < mHeight; row++) {
            for (int col = 0; col < mWidth; col++) {
                if ((pixels[row][col] < 0 || pixels[row][col] >= 256) && pixels[row][col] != MISSING_COLOR_VALUE) {
                    throw new InvalidImageException();
                }
            }
        }
    }
}
