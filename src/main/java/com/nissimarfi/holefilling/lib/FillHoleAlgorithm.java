package com.nissimarfi.holefilling.lib;

import com.nissimarfi.holefilling.lib.findholestrategy.FindHoleStrategy;
import com.nissimarfi.holefilling.lib.weight.WeightCalculator;

/**
 * A class that fill a hole in an image.
 */
public class FillHoleAlgorithm {

    private WeightCalculator mWeightCalculator;

    private FindHoleStrategy mFindHoleStrategy;

    public FillHoleAlgorithm(WeightCalculator weightCalculator,
                             FindHoleStrategy findHoleStrategy) {
        mWeightCalculator = weightCalculator;
        mFindHoleStrategy = findHoleStrategy;
    }

    public void fillHole(GrayImage image) {
        fillHole(image, false);
    }

    public void fillHole(GrayImage image, boolean showBoundary) {
        Hole hole = mFindHoleStrategy.find(image);

        if (hole == null) {
            return;
        }

        Iterable<Point> boundaryPoints = hole.getBoundaryPoints();
        Iterable<Point> holePoints = hole.getHolePoints();

        for (Point u : holePoints) {
            float sumWeightNumerator = 0f;
            float sumWeightDenominator = 0f;

            for (Point v : boundaryPoints) {
                float weight = mWeightCalculator.weight(u, v);

                float vPixelColor = image.getPixel(v.getX(), v.getY());

                sumWeightNumerator += weight * vPixelColor;
                sumWeightDenominator += weight;
            }

            float pixelColor = sumWeightNumerator / sumWeightDenominator;
            image.setPixel(u.getX(), u.getY(), pixelColor);
        }

        if (showBoundary) {
            for (Point p : boundaryPoints) {
                image.setGrayscale(p.getX(), p.getY(), 0x0);
            }
        }
    }
}
