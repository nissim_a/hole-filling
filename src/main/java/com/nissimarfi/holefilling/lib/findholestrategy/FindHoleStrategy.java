package com.nissimarfi.holefilling.lib.findholestrategy;

import com.nissimarfi.holefilling.lib.GrayImage;
import com.nissimarfi.holefilling.lib.Hole;
import com.nissimarfi.holefilling.lib.pixelconnectivity.PixelConnectivity;
import com.nissimarfi.holefilling.lib.Point;

import java.util.LinkedList;
import java.util.List;

/**
 * A class that find a hole in an image.
 */
public abstract class FindHoleStrategy {

    protected PixelConnectivity mPixelConnectivity;

    public FindHoleStrategy(PixelConnectivity pixelConnectivity) {
        mPixelConnectivity = pixelConnectivity;
    }

    public abstract Hole find(GrayImage image);

    protected boolean isBoundary(Point p, GrayImage image) {
        return !image.isHole(p.getX(), p.getY()) && getHoleNeighbors(p, image) != null;
    }

    protected Iterable<Point> getHoleNeighbors(Point p, GrayImage image) {
        Iterable<Point> neighbors = mPixelConnectivity.getNeighbors(p);

        List<Point> holeNeighbors = null;
        for (Point neighbor : neighbors) {
            try {
                if (image.isHole(neighbor.getX(), neighbor.getY())) {
                    if (holeNeighbors == null) {
                        holeNeighbors = new LinkedList<>();
                    }
                    holeNeighbors.add(neighbor);
                }
            } catch (IllegalArgumentException ignore) { }
        }
        return holeNeighbors;
    }
}
