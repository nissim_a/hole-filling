package com.nissimarfi.holefilling.lib.findholestrategy;

import com.nissimarfi.holefilling.lib.GrayImage;
import com.nissimarfi.holefilling.lib.Hole;
import com.nissimarfi.holefilling.lib.findfirstholestrategy.FindFirstHolePixelStrategy;
import com.nissimarfi.holefilling.lib.pixelconnectivity.PixelConnectivity;
import com.nissimarfi.holefilling.lib.Point;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class DFSFindHoleStrategy extends FindHoleStrategy {

    private FindFirstHolePixelStrategy mFindFirstHolePixelStrategy;

    public DFSFindHoleStrategy(PixelConnectivity pixelConnectivity, FindFirstHolePixelStrategy findFirstHolePixelStrategy) {
        super(pixelConnectivity);
        mFindFirstHolePixelStrategy = findFirstHolePixelStrategy;
    }

    @Override
    public Hole find(GrayImage image) {
        Point firstHolePixel = mFindFirstHolePixelStrategy.findFirstHolePixel(image);
        if (firstHolePixel == null) {
            return null;
        }

        Set<Point> boundaryPixels = new HashSet<>();
        Set<Point> holePixels = new HashSet<>();

        Stack<Point> stack = new Stack<>();
        stack.push(firstHolePixel);
        holePixels.add(firstHolePixel);

        while (!stack.isEmpty()) {
            Point currentHole = stack.pop();

            Iterable<Point> neighbors = mPixelConnectivity.getNeighbors(currentHole);
            for (Point neighbor : neighbors) {
                if (image.isHole(neighbor.getX(), neighbor.getY())) {
                    if (!holePixels.contains(neighbor)) {
                        stack.add(neighbor);
                        holePixels.add(neighbor);
                    }
                } else {
                    boundaryPixels.add(neighbor);
                }
            }
        }

        return new Hole(boundaryPixels, holePixels);
    }
}
