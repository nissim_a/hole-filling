package com.nissimarfi.holefilling.lib.findholestrategy;

import com.nissimarfi.holefilling.lib.GrayImage;
import com.nissimarfi.holefilling.lib.Hole;
import com.nissimarfi.holefilling.lib.pixelconnectivity.PixelConnectivity;
import com.nissimarfi.holefilling.lib.Point;

import java.util.HashSet;
import java.util.Set;

public class NaiveFindHoleStrategy extends FindHoleStrategy {

    public NaiveFindHoleStrategy(PixelConnectivity pixelConnectivity) {
        super(pixelConnectivity);
    }

    @Override
    public Hole find(GrayImage image) {
        Set<Point> boundaryPixels = new HashSet<>();
        Set<Point> holePixels = new HashSet<>();

        int width = image.getWidth();
        int height = image.getHeight();

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                Point point = new Point(col ,row);
                if (image.isHole(col, row)) {
                    holePixels.add(point);
                } else if (isBoundary(point, image)){
                    boundaryPixels.add(point);
                }
            }
        }
        return new Hole(boundaryPixels, holePixels);
    }
}