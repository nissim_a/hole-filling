package com.nissimarfi.holefilling.lib.pixelconnectivity;

import com.nissimarfi.holefilling.lib.Point;

import java.util.LinkedList;
import java.util.List;

/**
 * 4-connected pixels are neighbors to every pixel that touches one of their edges.
 * These pixels are connected horizontally and vertically.
 */
public class FourPixelConnectivity implements PixelConnectivity {

    @Override
    public List<Point> getNeighbors(Point p) {
        List<Point> neighbors = new LinkedList<>();
        neighbors.add(new Point(p.getX() + 1, p.getY()));
        neighbors.add(new Point(p.getX() - 1, p.getY()));
        neighbors.add(new Point(p.getX(), p.getY() + 1));
        neighbors.add(new Point(p.getX(), p.getY() - 1));
        return neighbors;
    }
}
