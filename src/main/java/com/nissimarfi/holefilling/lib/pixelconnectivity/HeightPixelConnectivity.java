package com.nissimarfi.holefilling.lib.pixelconnectivity;

import com.nissimarfi.holefilling.lib.Point;

import java.util.List;

/**
 * 8-connected pixels are neighbors to every pixel that touches one of their edges or corners.
 * These pixels are connected horizontally, vertically, and diagonally.
 */
public class HeightPixelConnectivity extends FourPixelConnectivity {

    @Override
    public List<Point> getNeighbors(Point p) {
        List<Point> neighbors = super.getNeighbors(p);
        neighbors.add(new Point(p.getX() + 1, p.getY() + 1));
        neighbors.add(new Point(p.getX() + 1, p.getY() - 1));
        neighbors.add(new Point(p.getX() - 1, p.getY() + 1));
        neighbors.add(new Point(p.getX() - 1, p.getY() - 1));
        return neighbors;
    }
}
