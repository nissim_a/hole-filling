package com.nissimarfi.holefilling.lib.pixelconnectivity;

import com.nissimarfi.holefilling.lib.Point;

/**
 * An interface that get the neighbors of a pixel
 */
public interface PixelConnectivity {

    Iterable<Point> getNeighbors(Point point);
}
