package com.nissimarfi.holefilling.lib.findfirstholestrategy;

import com.nissimarfi.holefilling.lib.GrayImage;
import com.nissimarfi.holefilling.lib.Point;

import java.util.Random;

public class RandomFirstPixelStrategy implements FindFirstHolePixelStrategy {

    @Override
    public Point findFirstHolePixel(GrayImage image) {
        int width = image.getWidth();
        int height = image.getHeight();

        int count = 0;

        Random r = new Random();
        while (true) {
            if (count > width * height) {
                FindFirstHolePixelStrategy f = new NaiveFirstPixelStrategy();
                return f.findFirstHolePixel(image);
            }

            count++;
            int x = r.nextInt(width);
            int y = r.nextInt(height);
            if (image.isHole(x, y)) {
                System.out.println("Found after " + count + " random");
                return new Point(x, y);
            }
        }
    }
}
