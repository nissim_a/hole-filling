package com.nissimarfi.holefilling.lib.findfirstholestrategy;

import com.nissimarfi.holefilling.lib.GrayImage;
import com.nissimarfi.holefilling.lib.Point;

public interface FindFirstHolePixelStrategy {

    Point findFirstHolePixel(GrayImage image);
}
