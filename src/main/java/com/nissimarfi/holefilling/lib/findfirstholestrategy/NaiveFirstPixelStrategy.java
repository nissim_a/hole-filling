package com.nissimarfi.holefilling.lib.findfirstholestrategy;

import com.nissimarfi.holefilling.lib.GrayImage;
import com.nissimarfi.holefilling.lib.Point;

public class NaiveFirstPixelStrategy implements FindFirstHolePixelStrategy {

    @Override
    public Point findFirstHolePixel(GrayImage image) {
        int width = image.getWidth();
        int height = image.getHeight();

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                if (image.isHole(col, row)) {
                    return new Point(col, row);
                }
            }
        }

        return null;
    }
}
