package com.nissimarfi.holefilling.lib.weight;

import com.nissimarfi.holefilling.lib.Point;

public class DefaultWeightCalculator implements WeightCalculator {

    private float mEpsilon;

    private float mZ;

    public DefaultWeightCalculator(float epsilon, float z) {
        mEpsilon = epsilon;
        mZ = z;
    }

    @Override
    public float weight(Point a, Point b) {
        float cx = a.getX() - b.getX();
        float cy = a.getY() - b.getY();
        double euclideanDist = Math.pow(cx, 2) + Math.pow(cy, 2);
        return 1 / (((float) Math.sqrt(Math.pow(euclideanDist, mZ))) + mEpsilon);
    }
}
