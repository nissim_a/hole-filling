package com.nissimarfi.holefilling.lib.weight;

import com.nissimarfi.holefilling.lib.Point;

/**
 * An interface to the weight function.
 */
public interface WeightCalculator {

    float weight(Point a, Point b);
}
