package com.nissimarfi.holefilling.cli;

import com.nissimarfi.holefilling.cli.bufferedimage.ConvertBufferedImageToGray;
import com.nissimarfi.holefilling.cli.bufferedimage.MergeBufferedImages;
import com.nissimarfi.holefilling.cli.utils.ArgsParser;
import com.nissimarfi.holefilling.cli.utils.ImageUtils;
import com.nissimarfi.holefilling.lib.*;
import com.nissimarfi.holefilling.lib.findfirstholestrategy.FindFirstHolePixelStrategy;
import com.nissimarfi.holefilling.lib.findfirstholestrategy.NaiveFirstPixelStrategy;
import com.nissimarfi.holefilling.lib.findfirstholestrategy.RandomFirstPixelStrategy;
import com.nissimarfi.holefilling.lib.findholestrategy.DFSFindHoleStrategy;
import com.nissimarfi.holefilling.lib.pixelconnectivity.FourPixelConnectivity;
import com.nissimarfi.holefilling.lib.pixelconnectivity.HeightPixelConnectivity;
import com.nissimarfi.holefilling.lib.pixelconnectivity.PixelConnectivity;
import com.nissimarfi.holefilling.lib.weight.DefaultWeightCalculator;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class Application {

    public static void main(String args[]) {
        HashMap<String, Object> argsHashMap = ArgsParser.parseArgs(args);
        String command = args.length > 0 ? args[0] : "";
        switch (command) {
            case "convert":
                convert(argsHashMap);
                break;
            case "fill-hole":
                fillHole(argsHashMap);
                break;
            case "merge":
                merge(argsHashMap);
                break;
            default:
                System.out.println("command not found");
                break;
        }
    }

    private static void convert(HashMap<String, Object> argsHashMap) {
        try {
            String colorSpace = (String) argsHashMap.get("-colorspace");
            if (colorSpace.equals("gray")) {
                String srcFilePath = (String) argsHashMap.get("-f");
                String dstFilePath = (String) argsHashMap.get("-d");

                File srcFile = new File(srcFilePath);
                File dstFile = new File(dstFilePath);
                dstFile.createNewFile();

                BufferedImage srcImage = ImageIO.read(srcFile);

                // Convert to gray
                ConvertBufferedImageToGray.convert(srcImage);

                GrayImage grayImage = ImageUtils.toImage(srcImage);

                BufferedImage buffDst = ImageUtils.toBufferedImage(grayImage);
                ImageIO.write(buffDst, "jpg", dstFile);

                System.out.println("converted");
            } else {
                System.out.println("colorspace not supported");
            }
        } catch (Exception e) {
            System.out.println("conversion failed");
        }
    }

    private static void merge(HashMap<String, Object> argsHashMap) {
        try {
            String srcFilePath = (String) argsHashMap.get("-f");
            String dstFilePath = (String) argsHashMap.get("-d");
            String maskFilePath = (String) argsHashMap.get("-m");

            File srcFile = new File(srcFilePath);
            File maskFile = new File(maskFilePath);
            File dstFile = new File(dstFilePath);
            dstFile.createNewFile();

            BufferedImage srcImage = ImageIO.read(srcFile);
            BufferedImage mask = ImageIO.read(maskFile);

            // Applying mask
            Color blackColor = new Color(0, 0, 0);
            MergeBufferedImages.merge(srcImage, mask, blackColor.getRGB(), -1);

            // Write image to disk
            ImageIO.write(srcImage, "jpg", dstFile);

            System.out.println("merged");
        } catch (IOException e) {
            System.out.println("merge failed");
        }
    }

    private static void fillHole(HashMap<String, Object> argsHashMap) {
        try {
            String srcFilePath = (String) argsHashMap.get("-f");
            String dstFilePath = (String) argsHashMap.get("-d");
            String maskFilePath = (String) argsHashMap.get("-m");

            String epsilonStr = (String) argsHashMap.get("-e");
            float epsilon = epsilonStr != null ? Float.parseFloat(epsilonStr) : 0.000001f;

            String zStr = (String) argsHashMap.get("-z");
            float z = zStr != null ? Float.parseFloat(zStr) : 5f;

            String connectivityStr = (String) argsHashMap.get("-c");
            int connectivity = connectivityStr != null ? Integer.parseInt(connectivityStr) : 8;

            boolean showBoundary = argsHashMap.get("-b") != null && (boolean) argsHashMap.get("-b");
            boolean random = argsHashMap.get("-r") != null && (boolean) argsHashMap.get("-r");

            File srcFile = new File(srcFilePath);
            File maskFile = new File(maskFilePath);
            File dstFile = new File(dstFilePath);
            dstFile.createNewFile();

            BufferedImage srcImage = ImageIO.read(srcFile);
            BufferedImage mask = ImageIO.read(maskFile);

            // Converts image to gray
            ConvertBufferedImageToGray.convert(srcImage);

            // Applying mask
            Color blackColor = new Color(0, 0, 0);
            MergeBufferedImages.merge(srcImage, mask, blackColor.getRGB(), -1);

            // Maps gray BufferedImage to GrayImage
            GrayImage grayImage = ImageUtils.toImage(srcImage);

            // Connectivity
            PixelConnectivity pixelConnectivity;
            switch (connectivity) {
                case 4:
                    pixelConnectivity = new FourPixelConnectivity();
                    break;
                case 8:
                    pixelConnectivity = new HeightPixelConnectivity();
                    break;
                default:
                    pixelConnectivity = new HeightPixelConnectivity();
                    System.out.println("connectivity " + connectivity + " not supported,  the default one is used.");
                    break;
            }

            // Filling the hole
            FindFirstHolePixelStrategy findFirstHolePixelStrategy =
                    random ? new RandomFirstPixelStrategy() : new NaiveFirstPixelStrategy();

            FillHoleAlgorithm fillHoleAlgorithm = new FillHoleAlgorithm(
                    new DefaultWeightCalculator(epsilon, z),
                    new DFSFindHoleStrategy(
                            pixelConnectivity,
                            findFirstHolePixelStrategy
                    )
//                    new NaiveFindHoleStrategy(pixelConnectivity)
            );
            fillHoleAlgorithm.fillHole(grayImage, showBoundary);

            // Maps GrayImage to BufferedImage
            BufferedImage buffDst = ImageUtils.toBufferedImage(grayImage);

            // Write image to disk
            ImageIO.write(buffDst, "jpg", dstFile);

            System.out.println("filled");
        } catch (IOException e) {
            System.out.println("filling hole failed");
        }
    }
}
