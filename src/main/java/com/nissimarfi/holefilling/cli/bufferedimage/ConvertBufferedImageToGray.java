package com.nissimarfi.holefilling.cli.bufferedimage;

import java.awt.image.BufferedImage;

public class ConvertBufferedImageToGray {

    public static void convert(BufferedImage image) {
        int height = image.getHeight();
        int width = image.getWidth();

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                int pixel = toGray(image.getRGB(col, row));
                pixel = (pixel << 16) | (pixel << 8) | pixel;
                image.setRGB(col, row, pixel);
            }
        }
    }

    // Returns a grayscale version of the given pixel.
    private static int toGray(int color) {
        int red = (color >> 16) & 0xff;
        int green = (color >> 8) & 0xff;
        int blue = color & 0xff;
        return  Math.round((red + green + blue) / 3f);
    }
}
