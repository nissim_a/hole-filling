package com.nissimarfi.holefilling.cli.bufferedimage;

import java.awt.image.BufferedImage;

public class MergeBufferedImages {

    public static void merge(BufferedImage image, BufferedImage mask, int maskColor, int maskValue) {
        if (image.getHeight() != mask.getHeight() || image.getWidth() != mask.getWidth()) {
            throw new IllegalArgumentException("The two images need to have the same dimensions");
        }

        int height = image.getHeight();
        int width = image.getWidth();

        maskColor = maskColor & 0x00ffffff;
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                int maskPixel = mask.getRGB(col, row) & 0x00ffffff;
                if (maskColor == maskPixel) {
                    image.setRGB(col, row, maskValue);
                }
            }
        }
    }
}
