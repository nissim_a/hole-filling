package com.nissimarfi.holefilling.cli.utils;

import com.nissimarfi.holefilling.lib.GrayImage;

import java.awt.image.BufferedImage;

public class ImageUtils {

    public static GrayImage toImage(BufferedImage bufferedImage) {
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();

        GrayImage image = new GrayImage(width, height);

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                byte pixel = (byte) bufferedImage.getRGB(col, row);
                image.setGrayscale(col, row, pixel);
            }
        }

        return image;
    }

    public static BufferedImage toBufferedImage(GrayImage grayImage) {
        int width = grayImage.getWidth();
        int height = grayImage.getHeight();

        BufferedImage bufferedImage = new BufferedImage(
            width,
            height,
            BufferedImage.TYPE_INT_RGB
        );

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                int gray = grayImage.getGrayscale(col, row);
                gray = 0xff000000 | (gray << 16) | (gray << 8) | gray;
                bufferedImage.setRGB(col, row, gray);
            }
        }

        return bufferedImage;
    }
}
