package com.nissimarfi.holefilling.cli.utils;

import java.util.HashMap;

public class ArgsParser {

    public static HashMap<String, Object> parseArgs(String args[]) {
        HashMap<String, Object> hashMap = new HashMap<>();
        for (int i = 1; i < args.length; i++) {
            String currentVal = args[i];
            String nextVal = i + 1 < args.length ? args[i + 1] : null;

            if (isAFlag(currentVal)) {
                if (nextVal == null || isAFlag(nextVal)) {
                    hashMap.put(currentVal, true);
                } else {
                    hashMap.put(currentVal, nextVal);
                    i++;
                }
            }
        }
        return hashMap;
    }

    private static boolean isAFlag(String val) {
        return val.startsWith("-");
    }
}
